<?php

namespace mef\UnitTest\Validation;

use mef\Validation\Type\StringType;

require_once __DIR__ . '/../MefworksUnitTest.php';

class SanitizationChainTest extends \MefworksTestCase
{
    public function testEmptyChain()
    {
        $chain = new \mef\Validation\Sanitizer\ChainSanitizer();

        $this->assertEquals([], $chain->getSanitizers());
        $this->assertEquals(42, $chain->sanitize(42));
    }

    public function testTrimmedString()
    {
        $chain = new \mef\Validation\Sanitizer\ChainSanitizer([
            new StringType(),
            new \mef\Validation\Sanitizer\TrimSanitizer()
        ]);

        foreach ($chain as $sanitizer) {
            $this->assertTrue($sanitizer instanceof \mef\Validation\Sanitizer\SanitizationInterface);
        }

        $this->assertEquals(2, count($chain->getSanitizers()));
        $this->assertEquals('42', $chain->sanitize(42));
        $this->assertEquals('trimmed', $chain->sanitize('  trimmed  '));
    }

    public function testInvalidSanitizer()
    {
        $this->expectException(\mef\Validation\Exception\InvalidArgumentException::class);
        $chain = new \mef\Validation\Sanitizer\ChainSanitizer([42]);
    }
}
