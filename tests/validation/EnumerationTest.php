<?php

namespace mef\UnitTest\Validation;

use mef\Validation\Type\EnumerationType;

require_once __DIR__ . '/../MefworksUnitTest.php';

class EnumerationTest extends \MefworksTestCase
{
    public function testEnumeration()
    {
        $enum = new EnumerationType();
        $this->assertEquals([], $enum->getValues());

        $enum = new EnumerationType(['a','b','c']);
        $this->assertEquals(['a','b','c'], $enum->getValues());

        $this->assertEquals('a', $enum->sanitize('a'));

        $this->assertTrue($enum->validate('a'));
        $this->assertFalse($enum->validate('d'));
    }

    public function testInvalidEnumeration()
    {
        $this->expectException(\mef\Validation\Exception\IllegalCastException::class);
        $enum = new EnumerationType(['a','b','c']);
        $this->assertNull($enum->sanitize('d'));
    }
}
