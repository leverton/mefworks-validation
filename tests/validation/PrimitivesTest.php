<?php

namespace mef\UnitTest\Validation;

use stdClass;
use mef\Validation\Type\NullType;
use mef\Validation\Type\BooleanType;
use mef\Validation\Type\IntegerType;
use mef\Validation\Type\FloatType;
use mef\Validation\Type\StringType;
use mef\Validation\Type\PHPArrayType;

require_once __DIR__ . '/../MefworksUnitTest.php';

class PrimitivesTest extends \MefworksTestCase
{
    public function testNull()
    {
        $tests = [
            NullType::NULL => null,
            NullType::EMPTY_STRING => '',
            NullType::EMPTY_ARRAY => [],
            NullType::INTEGER => 0,
            NullType::FLOAT => 0.0,
            NullType::BOOLEAN => false,
            NullType::ZERO_STRING => '0'
        ];

        $values = array_values($tests);
        unset($values[0]);

        foreach ($tests as $flag => $true_test) {
            $null = new NullType($flag);

            $this->assertNull($null->sanitize(null));
            $this->assertNull($null->sanitize($true_test));

            $this->assertSame($true_test === null, $null->validate($true_test));
        }
    }

    public function testInvalidNull()
    {
        $this->expectException(\mef\Validation\Exception\IllegalCastException::class);
        $test = new NullType();
        $test->sanitize(42);
    }

    public function testBoolean()
    {
        $boolean = new BooleanType();

        $this->assertTrue($boolean->sanitize(true));
        $this->assertFalse($boolean->sanitize(false));
        $this->assertTrue($boolean->sanitize(1));

        $this->assertTrue($boolean->validate(true));
        $this->assertFalse($boolean->validate(0));
    }

    public function testInvalidBooleanArray()
    {
        $this->expectException(\mef\Validation\Exception\IllegalCastException::class);
        $test = new BooleanType();
        $test->sanitize([]);
    }

    public function testInvalidBooleanString()
    {
        $this->expectException(\mef\Validation\Exception\IllegalCastException::class);
        $test = new BooleanType();
        $test->sanitize('foo');
    }

    public function testInteger()
    {
        $integer = new IntegerType();

        $this->assertEquals(1, $integer->sanitize(1));
        $this->assertEquals(1, $integer->sanitize(true));
        $this->assertEquals(0, $integer->sanitize(false));
        $this->assertEquals(1, $integer->sanitize('1'));

        $this->assertTrue($integer->validate(1));
        $this->assertFalse($integer->validate('1'));
    }

    public function testInvalidIntegerString()
    {
        $this->expectException(\mef\Validation\Exception\IllegalCastException::class);
        $integer = new IntegerType();

        $this->assertEquals(1, $integer->sanitize('abc'));
    }

    public function testFloat()
    {
        $float = new FloatType();

        $this->assertEquals(1.0, $float->sanitize(1));

        $this->assertTrue($float->validate(1.0));
        $this->assertFalse($float->validate(1));
    }

    public function testInvalidFloatArray()
    {
        $this->expectException(\mef\Validation\Exception\IllegalCastException::class);
        $test = new FloatType();
        $test->sanitize([]);
    }

    public function testInvalidFloatString()
    {
        $this->expectException(\mef\Validation\Exception\IllegalCastException::class);
        $test = new FloatType();
        $test->sanitize('foo');
    }

    public function testString()
    {
        $string = new StringType();

        $this->assertEquals('1', $string->sanitize(1));
        $this->assertEquals('a', $string->sanitize('a'));

        $this->assertTrue($string->validate('1'));
        $this->assertFalse($string->validate(1));
    }

    public function testObjectToString()
    {
        $this->expectException(\mef\Validation\Exception\IllegalCastException::class);
        $string = new StringType();
        $string->sanitize(new stdClass());
    }

    public function testArray()
    {
        $array = new PHPArrayType();

        $this->assertEquals([], $array->sanitize([]));

        $this->assertTrue($array->validate([]));
        $this->assertFalse($array->validate(1));
    }

    public function testScalarToArray()
    {
        $this->expectException(\mef\Validation\Exception\IllegalCastException::class);
        $array = new PHPArrayType();
        $array->sanitize(1);
    }
}
