<?php

namespace mef\Validation\Sanitizer;

use mef\Validation\Type\EnumerationType;
use mef\Validation\Exception\IllegalCastException;
use mef\Validation\Sanitizer\SanitizationInterface;
use mef\Validation\Exception\InvalidTemplateKeyException;
use mef\Validation\Exception\InvalidTemplateTypeException;
use mef\Validation\Exception\DuplicateTemplateKeyException;

/**
 * The Template filter sanitizes an associative array according to the
 * specified template. If you need to sanitize an indexed array of data
 * all against the same sanitizer object, see mef\Stdlib\ArrayUtil::sanitize().
 *
 * There are two pieces of configuration that are needed:
 *
 *   1) a mef\Validation\TemplateTypeMapInterface object - This is able to
 *      return a filter based on a string value. e.g., 'string' may be mapped to
 *      a chain of mef\Validation\UTF8String and mef\Validation\Trim. If no type
 *      class is specified, then an instance of mef\Validation\TypeMap is used.
 *
 *   2) an array (template) that defines what shape the output will be in.
 *
 *      The template filter guarantees that the output will be an associative
 *      array with exactly the requested fields, and no more. If any keys are
 *      missing, an empty value is used in its place. Excess keys are ignored.
 *
 *      The values of template keys can be several things:
 *
 *      * string: calls the associated mef\Validation\TemplateTypeInterface
 *        object to determine the type
 *
 *      * mef\Validation\Sanitizer\SanitizationInterface: calls the filter
 *        directly
 *
 *      * callable object / function: calls the function directly
 *
 *      * indexed array: an enumeration of allowed values
 *
 *      * associative array: a sub-template
 *
 *      If the key ends with [type] then the filter is applied over an array of
 *      input values, sanitizing the keys against the 'type' sanitizer.
 *      If 'type' is omitted (e.g., 'foo[]'), then keys are ignored, and an
 *      indexed array of non-null values from from 0 to n-1 is built.
 *
 * <code>
 * $template = [
 *   'person[]' => [
 *     'name' => 'string',
 *     'age' => 'integer',
 *     'gender' => ['male', 'female']
 *   ],
 *   'color[string]' => [
 *     'r' => 'float',
 *     'g' => 'float',
 *     'b' => 'float'
 *   ],
 *   'accepts_terms' => 'boolean'
 * ];
 *
 * $input = [
 *   'person' => [
 *      ['name' => 'John Doe', 'age' => '42' ],
 *      42 => ['name' => 'Jane Doe', 'gender' => 'female']
 *   ],
 *   'color' => [
 *     'red' => ['r' => 1, 'g' => 0, 'b' => 0 ]
 *   ],
 *   'accepts_terms' => '1'
 * ]
 *
 * $sanitizer = new mef\Validation\Template($template);
 * $output = $sanitizer->sanitize($input);
 * </code>
 *
 * The output is:
 *
 * array(
 *   'person' => [
 *     0 => ['name' => 'John Doe', 'age' => 42, 'gender' => null],
 *     1 => ['name' => 'Jane Doe', 'age' => null, 'gender' => female]
 *   ],
 *   'color' => [
 *     'red' => ['r' => 1.0, 'g' => 0.0, 'b' => 0.0 ]
 *   ],
 *   'accepts_terms' => true
 * )
 *
 */
class TemplateSanitizer implements SanitizationInterface
{
    private array $typeCache = [];

    /**
     * Constructor
     *
     * @param array $template   See class documentation for format
     * @param \mef\Validation\Sanitizer\TemplateTypeMapInterface $typeMap  The object
     *           that resolves a type string into a sanitization object. If not
     *           supplied, a mef\Validation\TypeMap is used.
     */
    public function __construct(private array $template, private TemplateTypeMapInterface $typeMap = new TypeMap())
    {
        $this->template = $this->parseTemplate($template);
    }

    /**
     * Returns the TypeMap.
     *
     * @return \mef\Validation\Sanitizer\TemplateTypeMapInterface
     */
    public function getTypeMap(): TemplateTypeMapInterface
    {
        return $this->typeMap;
    }

    /**
     * Returns the template.
     *
     * @return array
     */
    public function getTemplate(): array
    {
        return $this->template;
    }

    /**
     * Sanitizes the input per the specifications of the template and the
     * TypeMap as configured via the constructor.
     *
     * @param mixed $input   The input to sanitize. This must be an array.
     *
     * @return array   The sanitized output
     */
    public function sanitize(mixed $input): mixed
    {
        if (is_array($input) === false) {
            throw new IllegalCastException();
        }

        return $this->sanitizeWithResults($input)['output'];
    }

    /**
     * Sanitizes the input per the specifications of the template and the
     * TypeMap as configured via the constructor. In addition, it returns
     * any errors.
     *
     * @param array $input The input to validate.
     *
     * @return array
     */
    public function sanitizeWithResults(array $input): array
    {
        $output = [];
        $errors = [];

        // Iterate over the $template, because that defines the desired
        // output. $input is compared against the template.
        foreach ($this->template as $key => $type) {
            if (isset($input[$key]) === true) {
                $value = $input[$key];
            } elseif ($type->isRequired === true) {
                throw new IllegalCastException("$key is required");
            } elseif ($type->skipMissing === true) {
                continue;
            } else {
                $value = $type->isArray === true ? [] : null;
            }

            try {
                if (is_callable([$type->filter, 'sanitizeWithResults']) === true) {
                    $results = $type->filter->sanitizeWithResults($value);
                    $output[$key] = $results['output'];
                    $errors[$key] = $results['errors'];
                } else {
                    $output[$key] = $type->filter->sanitize($value);
                }
            } catch (IllegalCastException $e) {
                $output[$key] = $type->isArray === true ? [] : null;
                $errors[$key] = $e;
            }

            if (($output[$key] === null || $output[$key] === '' || $output[$key] === []) && $type->isRequired === true) {
                throw new IllegalCastException("$key is required");
            }

            if ($type->isArray === true) {
                if (
                    ($type->minimum !== null && count($output[$key]) < $type->minimum) ||
                    ($type->maximum !== null && count($output[$key]) > $type->maximum)
                ) {
                    throw new IllegalCastException("$key does not have the right amount of elements");
                }

                if ($type->arrayType === '') {
                    $output[$key] = array_values($output[$key]);
                }
            }
        }

        return [
            'input' => $input,
            'output' => $output,
            'errors' => $errors
        ];
    }

    /**
     * Parse the user template into a directly usable template
     * for sanitizing.
     *
     * @param array $template  The input template
     *
     * @return array The parsed template
     */
    private function parseTemplate(array $template): array
    {
        $parsedTemplate = [];

        foreach ($template as $key => $type) {
            $filter = $this->parseType($type);

            if (
                !preg_match('/^
				(?<key>[^\[\]*?]+)    # one or more non []*? chars
				(?<array>\[           # optional, uncaptured group beginning with [
				(?<type>[^\[\]]*)   # ... containing zero or more non [] chars
				\])?                  # ... ending with ]
				(?<req>(\?|\*|
				{
					(?<min>0|[1-9][0-9]*)        # minimum
					(,(?<max>([1-9][0-9]*)))?    # maximum
				}
				))?      # optional is-required marker
				$/x', (string) $key, $m)
            ) {
                throw new InvalidTemplateKeyException();
            }

            $key = $m['key'];

            if (isset($parsedTemplate[$key]) === true) {
                throw new DuplicateTemplateKeyException();
            }

            $isArray = isset($m['array']) && $m['array'] !== '';
            $isRequired = false;
            $skipMissing = false;
            $minimum = null;
            $maximum = null;

            if (isset($m['req']) === true && $m['req'] !== '') {
                if ($m['req'] === '*') {
                    $isRequired = true;
                    $minimum = 1;
                } elseif ($m['req'] === '?') {
                    $skipMissing = true;
                } elseif ($isArray === false) {
                    throw new InvalidTemplateKeyException();
                } else {
                    $minimum = $m['min'];
                    $maximum = isset($m['max']) ? $m['max'] : null;

                    if ($maximum !== null && $maximum < $minimum) {
                        throw new InvalidTemplateKeyException();
                    }
                }
            }

            if ($isArray === true) {
                $filter = new ArraySanitizer($filter, $m['type'] === '' ? null : $this->typeMap->getSanitizer($m['type']));
            }

            $parsedTemplate[$key] = (object) [
                'filter' => $filter,
                'isArray' => $isArray,
                'isRequired' => $isRequired,
                'skipMissing' => $skipMissing,
                'minimum' => $minimum,
                'maximum' => $maximum,
                'arrayType' => $m['type'] ?? '',
            ];
        }

        return $parsedTemplate;
    }

    /**
     * Parse the right hand side of the template, and return an appropriate
     * sanitization object.
     *
     * @param mixed $type  Any valid right-hand-side value of a template.
     *                     i.e., a string type name, an enumeration array, a
     *                     sub-template array, callback, or sanitization object.
     *
     * @return \mef\Validation\Sanitizer\SanitizationInterface
     */
    private function parseType(mixed $type): SanitizationInterface
    {
        if (is_string($type)) {
            if (!isset($this->typeCache[$type])) {
                $this->typeCache[$type] = $this->typeMap->getSanitizer($type);
            }

            return $this->typeCache[$type];
        } elseif (is_array($type)) {
            if (array_is_list($type)) {
                $filter = new EnumerationType($type);
            } else {
                $filter = new TemplateSanitizer($type, $this->typeMap);
                $filter->typeCache = $this->typeCache;
            }
        } elseif ($type instanceof SanitizationInterface) {
            $filter = $type;
        } elseif (is_callable($type)) {
            $filter = new CallbackSanitizer($type);
        } else {
            throw new InvalidTemplateTypeException();
        }

        return $filter;
    }
}
