<?php

namespace mef\Validation\Sanitizer;

/**
 * If null or empty string, return some other value.
 */
class DefaultValueSanitizer implements SanitizationInterface
{
    /**
     * Constructor
     *
     * @param mixed $defaultValue   Used as the replacement for null or empty
     *                              string.
     */
    public function __construct(private mixed $defaultValue)
    {
    }

    /**
     * Return the default value.
     *
     * @return mixed
     */
    public function getDefaultValue(): mixed
    {
        return $this->defaultValue;
    }

    /**
     * If the value is null or empty string, then the defined default value
     * will be returned instead.
     *
     * @param  mixed $value
     *
     * @return mixed
     */
    public function sanitize(mixed $value): mixed
    {
        return $value === null || $value === '' ? $this->defaultValue : $value;
    }
}
