<?php

namespace mef\Validation\Sanitizer;

use mef\Validation\Type;
use mef\Validation\Exception\InvalidArgumentException;

class TypeMap implements TemplateTypeMapInterface
{
    public function getSanitizer(string $type): SanitizationInterface
    {
        return match($type) {
            'float!' =>
                new ChainSanitizer([
                    new DefaultValueSanitizer(0.0),
                    new Type\FloatType()
                ]),

            'integer!' =>
                new ChainSanitizer([
                    new DefaultValueSanitizer(0),
                    new Type\IntegerType()
                ]),

            'boolean!' =>
                new ChainSanitizer([
                    new DefaultValueSanitizer(false),
                    new Type\BooleanType()
                ]),

            'string!' =>
                new ChainSanitizer([
                    new DefaultValueSanitizer(''),
                    new Type\MultibyteStringType()
                ]),

            'string' => new Type\MultibyteStringType(),

            'integer' => new Type\IntegerType(),

            'boolean' => new Type\BooleanType(),

            'float' => new Type\FloatType(),

            'datetime' => new Type\DateTimeType(),

            default => throw new InvalidArgumentException("Unknown type $type")
        };
    }
}
