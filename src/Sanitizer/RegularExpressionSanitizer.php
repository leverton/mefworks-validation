<?php

namespace mef\Validation\Sanitizer;

use mef\Validation\Exception\IllegalCastException;

class RegularExpressionSanitizer implements SanitizationInterface
{
    /**
     * Constructor
     *
     * @param string $expression        The regulat rexpression (see preg_*)
     * @param string|null $replacement  The replacement values to use, if any.
     *                                  Use null for no replacement.
     */
    public function __construct(private string $expression, private ?string $replacement = null)
    {
        $this->expression = $expression;
        $this->replacement = $replacement;
    }

    /**
     * Return the regular expression.
     *
     * @return string
     */
    public function getExpression(): string
    {
        return $this->expression;
    }

    /**
     * Return the replacement value (if any).
     *
     * @return string|null
     */
    public function getReplacement(): ?string
    {
        return $this->replacement;
    }

    /**
     * Validate that the value matches the regular expression.
     *
     * @param  mixed $value
     *
     * @return mixed
     * @throws \mef\Validation\Exception\IllegalCastException
     */
    public function sanitize(mixed $value): mixed
    {
        if ($this->replacement === null) {
            if (!preg_match($this->expression, $value)) {
                throw new IllegalCastException();
            }

            return $value;
        } else {
            return preg_replace($this->expression, $this->replacement, $value);
        }
    }
}
