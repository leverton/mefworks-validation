<?php

namespace mef\Validation\Sanitizer;

interface TemplateTypeMapInterface
{
    public function getSanitizer(string $type): SanitizationInterface;
}
