<?php

namespace mef\Validation\Sanitizer;

interface SanitizationInterface
{
    public function sanitize(mixed $value): mixed;
}
