<?php

namespace mef\Validation\Sanitizer;

use ArrayIterator;
use IteratorAggregate;
use mef\Validation\Exception\InvalidArgumentException;
use Traversable;

/**
 * An immutable set of sanitization filters to run in-order.
 */

class ChainSanitizer implements SanitizationInterface, IteratorAggregate
{
    /**
     * Constructor
     *
     * @param array<SanitizationInterface> $sanitizers
     */
    public function __construct(private array $sanitizers = [])
    {
        foreach ($sanitizers as $sanitizer) {
            if (!$sanitizer instanceof SanitizationInterface) {
                throw new InvalidArgumentException('Every element must be a SanitizationInterface');
            }
        }
    }

    /**
     * Return an iterator that iterates over each sanitizer
     *
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->sanitizers);
    }

    /**
     * Return an array of sanitizers
     *
     * @return array
     */
    public function getSanitizers(): array
    {
        return $this->sanitizers;
    }

    /**
     * Pass the value through each of the sanitizers.
     *
     * @param mixed $value   The value to sanitize
     *
     * @return mixed  The sanitized value
     */
    public function sanitize(mixed $value): mixed
    {
        foreach ($this->sanitizers as $sanitizer) {
            $value = $sanitizer->sanitize($value);
        }

        return $value;
    }
}
