<?php

namespace mef\Validation\Sanitizer;

class TrimSanitizer implements SanitizationInterface
{
    public function __construct(private string $charlist = " \t\n\r\0\x0B")
    {
    }

    public function sanitize(mixed $value): string
    {
        return trim((string) $value, $this->charlist);
    }
}
