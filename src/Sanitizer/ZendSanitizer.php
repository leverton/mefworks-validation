<?php

namespace mef\Validation\Sanitizer;

use Zend\Filter\FilterInterface;

/**
 * An adapter that allows Zend filters to work with any method that expects
 * a mef filter.
 *
 * <code>
 * $stringFilter = new \mef\Validation\Type\UTF8StringType;
 * $nullFilter = new \mef\Validation\Sanitizer\ZendSanitizer(new \Zend\Filter\Null());
 *
 * $chainFilter = new \mef\Validation\Sanitizer\ChainSanitizer([$stringFilter, $nullFilter]);
 * var_dump($chainFilter->filter(0));
 * </code>
 *
 */
class ZendSanitizer implements SanitizationInterface
{
    /**
     * @param \Zend\Filter\FilterInterface $filter   The zend filter to wrap
     */
    public function __construct(private FilterInterface $filter)
    {
    }

    /**
     * Return the associated Zend filter.
     *
     * @return \Zend\Filter\FilterInterface
     */
    public function getZendFilter(): FilterInterface
    {
        return $this->filter;
    }

    /**
     * Filters the value by passing it through to the Zend filter as defined
     * in the constructor.
     *
     * @param mixed $value  The value to filter
     *
     * @return mixed
     */
    public function sanitize(mixed $value): mixed
    {
        return $this->filter->filter($value);
    }
}
