<?php

namespace mef\Validation\Sanitizer;

use Traversable;
use mef\Validation\Exception\IllegalCastException;

class ArraySanitizer implements SanitizationInterface
{
    public function __construct(private SanitizationInterface $valueSanitizer, private ?SanitizationInterface $keySanitizer = null)
    {
    }

    public function sanitize(mixed $values): array
    {
        return $this->sanitizeWithResults($values)['output'];
    }

    public function sanitizeWithResults(array | Traversable $values): array
    {
        $output = [];
        $errors = [];

        $index = 0;

        foreach ($values as $inputKey => $value) {
            try {
                $value = $this->valueSanitizer->sanitize($value);
            } catch (IllegalCastException $e) {
                $errors[$inputKey]['value'] = $e;
            }

            if ($this->keySanitizer !== null) {
                try {
                    $key = $this->keySanitizer->sanitize($inputKey);
                } catch (IllegalCastException $e) {
                    $errors[$inputKey]['key'] = $e;
                }
            } else {
                $key = $index++;
            }

            if (isset($errors[$inputKey]) === false) {
                $output[$key] = $value;
            }
        }

        return [
            'input' => $values,
            'output' => $output,
            'errors' => $errors,
        ];
    }
}
