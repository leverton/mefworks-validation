<?php

namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

class PHPArrayType implements TypeInterface
{
    /**
     * Casts the value to an array.
     *
     * @param mixed $value   The value to cast to an array
     *
     * @return array    The array representation of $value
     */
    public function sanitize(mixed $value): array
    {
        if (is_array($value) === false) {
            throw new IllegalCastException();
        }

        return $value;
    }

    /**
     * Validates that the value is an array.
     *
     * @param mixed $value   The value to test
     *
     * @return bool  true if $value is an array
     */
    public function validate(mixed $value): bool
    {
        return is_array($value);
    }
}
