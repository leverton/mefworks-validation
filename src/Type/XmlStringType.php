<?php

namespace mef\Validation\Type;

class XmlStringType extends MultibyteStringType
{
    private const PATTERN = '[^\x{09}\x{0A}\x{0D}\x{20}-\x{D7FF}\x{E000}-\x{FFFD}\x{10000}-\x{10FFFF}]';

    public function sanitize(mixed $value): string
    {
        $value = parent::sanitize($value);

        mb_regex_encoding($this->getEncoding());
        return mb_ereg_replace(self::PATTERN, '', $value);
    }

    public function validate(mixed $value): bool
    {
        if (parent::validate($value) === false) {
            return false;
        }

        mb_regex_encoding($this->getEncoding());
        return !mb_ereg_match('.*' . self::PATTERN, $value);
    }
}
