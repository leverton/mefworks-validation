<?php

namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

class EnumerationType implements TypeInterface
{
    public function __construct(private array $values = [])
    {
        $this->values = array_flip($values);
    }

    public function getValues(): array
    {
        return array_keys($this->values);
    }

    public function sanitize(mixed $value): string
    {
        if (isset($this->values[$value]) === false) {
            throw new IllegalCastException();
        }

        return $value;
    }

    public function validate(mixed $value): bool
    {
        return isset($this->values[$value]);
    }
}
