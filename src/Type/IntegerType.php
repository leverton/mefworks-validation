<?php

namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

class IntegerType implements TypeInterface
{
    /**
     * Casts the value to an integer.
     *
     * @param mixed $value   The value to cast to an integer
     *
     * @return int    The integer representation of $value
     */
    public function sanitize(mixed $value): int
    {
        if (is_int($value) === true) {
            return $value;
        } elseif (is_string($value) === true) {
            if (preg_match('/^-?(0|[1-9][0-9]*)$/', $value)) {
                return (int) $value;
            }
        } elseif ($value === true) {
            return 1;
        } elseif ($value === false) {
            return 0;
        }

        throw new IllegalCastException();
    }

    /**
     * Validates that the value is an integer type.
     *
     * @param mixed $value   The value to test
     *
     * @return bool  true if $value is an integer
     */
    public function validate(mixed $value): bool
    {
        return is_int($value);
    }
}
