<?php

namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

class FloatType implements TypeInterface
{
    /**
     * Casts the value to a float.
     *
     * @param mixed $value   The value to cast to a float
     *
     * @return float    The float representation of $value
     */
    public function sanitize(mixed $value): float
    {
        if (is_scalar($value) === false) {
            throw new IllegalCastException();
        } elseif (is_string($value) === true && !preg_match('/^-?(0|[1-9][0-9]*)(\\.[0-9]+)?$/', $value)) {
            throw new IllegalCastException("$value is not a valid string representation for a float");
        }

        return (float) $value;
    }

    /**
     * Validates that the value is a float type.
     *
     * @param mixed $value   The value to test
     *
     * @return bool  true if $value is a float
     */
    public function validate(mixed $value): bool
    {
        return is_float($value);
    }
}
