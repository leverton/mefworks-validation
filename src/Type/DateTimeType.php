<?php

namespace mef\Validation\Type;

use DateTimeInterface;
use DateTimeImmutable;
use mef\Validation\Exception\IllegalCastException;

class DateTimeType implements TypeInterface
{
    /**
     * Constructor
     *
     * @param string|null $inputFormat  The date format of the data
     */
    public function __construct(private ?string $inputFormat = null)
    {
    }

    /**
     * @return string
     */
    public function getInputFormat(): ?string
    {
        return $this->inputFormat;
    }

    /**
     * Return a DateTimeImmutable object from the value.
     *
     * @param  string $value
     * @return \DateTimeImmutable
     */
    public function sanitize(mixed $value): DateTimeImmutable
    {
        if (is_string($value) === true && $value !== '') {
            if ($this->inputFormat === null) {
                $value = date_create_immutable($value);
            } else {
                $value = date_create_immutable_from_format($this->inputFormat, $value);
            }

            if ($value === false) {
                throw new IllegalCastException();
            }
        } elseif ($value instanceof DateTimeInterface) {
            if (!($value instanceof DateTimeImmutable)) {
                $value = DateTimeImmutable::createFromInterface($value);
            }
        } else {
            throw new IllegalCastException();
        }

        return $value;
    }

    public function validate(mixed $value): bool
    {
        return $value instanceof DateTimeImmutable;
    }
}
