<?php

namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

class StringType implements TypeInterface
{
    /**
     * Casts the value to a string.
     *
     * @param mixed $value   The value to cast to a string
     *
     * @return string    The string representation of $value
     */
    public function sanitize(mixed $value): string
    {
        if (is_string($value) === true) {
            return $value;
        } elseif (is_scalar($value) === true || (is_object($value) === true && method_exists($value, '__toString'))) {
            return (string) $value;
        }

        throw new IllegalCastException();
    }

    /**
     * Validates that the value is a string type.
     *
     * @param mixed $value   The value to test
     *
     * @return bool  true if $value is a string
     */
    public function validate(mixed $value): bool
    {
        return is_string($value);
    }
}
