<?php

namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

class MultibyteStringType implements TypeInterface
{
    public function __construct(private string $encoding = 'UTF-8')
    {
    }

    public function getEncoding(): string
    {
        return $this->encoding;
    }

    public function sanitize(mixed $value): string
    {
        if (is_string($value)) {
            if (mb_check_encoding($value, $this->encoding) === false) {
                throw new IllegalCastException();
            }

            return $value;
        } elseif (is_scalar($value) === true) {
            return (string) $value;
        } elseif (is_object($value) === true && method_exists($value, '__toString') === true) {
            return $this->sanitize((string) $value);
        }

        throw new IllegalCastException();
    }

    public function validate(mixed $value): bool
    {
        return is_string($value) === true && mb_check_encoding($value, $this->encoding) === true;
    }
}
