<?php

namespace mef\Validation\Exception;

class InvalidTemplateTypeException extends \Exception implements ExceptionInterface
{
}
