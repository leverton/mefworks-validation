<?php

namespace mef\Validation\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
