<?php

namespace mef\Validation\Exception;

class DuplicateTemplateKeyException extends InvalidTemplateKeyException
{
}
