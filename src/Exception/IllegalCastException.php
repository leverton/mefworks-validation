<?php

namespace mef\Validation\Exception;

class IllegalCastException extends \Exception implements ExceptionInterface
{
}
