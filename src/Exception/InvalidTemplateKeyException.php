<?php

namespace mef\Validation\Exception;

class InvalidTemplateKeyException extends \Exception implements ExceptionInterface
{
}
